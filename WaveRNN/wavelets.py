import pywt
import librosa
import numpy as np
import config
from scipy.fftpack import dct
from scipy.signal import get_window
import soundfile as sf
from scipy.io import wavfile
import tensorflow as tf
import os
import glob
'''
Dictionary for the wavelet packet spectral bands.
We choose those in order to approach the mel scale spacing. 
'''
mel_wavelet_bands = {
    0: "aaaaaa",
    1: "aaaaad",
    2: "aaaada",
    3: "aaaadd",
    4: "aaadaa",
    5: "aaadad",
    6: "aaadda",
    7: "aaaddd",
    8: "aadaa",
    9: "aadad",
    10: "aadda",
    11: "aaddd",
    12: "adaaa",
    13: "adaad",
    14: "adada",
    15: "adadd",
    16: "addaa",
    17: "addad",
    18: "addd",
    19: "daaa",
    20: "daad",
    21: "dad",
    22: "dda",
    23: "ddd"
}


def waveletPacketTransform(audio_file_absolute_path):
    '''
    Wavelet packet trasform function.

    Parameters
    ----------
    audio_file_absolute_path : str
        Path of a single audio file.
    Output
    ------
        wp : pywavelet object
        Wavelet packet trasform of the given signal.
    '''
    # Load & Normalize signal
    signal, sr = librosa.load(audio_file_absolute_path, sr=config.sr)
    signal = librosa.util.normalize(signal)

    # Calculate wavelet packet trasform for input signal
    wp = pywt.WaveletPacket(data=signal,
                            wavelet=config.wavelet,
                            mode='symmetric')
    return wp


def frame_time_series(time_series, fr_length, hop_length):
    '''
    A function to divide signal into overlaping frames.
      
    Parameters
    ----------
    time_series : nympy array
        Time series array to frame.
    fr_length : int 
        Frame length. 
    hop_length : int 
        Number of samples to hop in each overlaping frame.
    Output
    ------
    framed_time_series : np.array (no_frames, fr_length)
        The framed signal
    '''
    # Pad signal
    padded_series = np.pad(time_series, hop_length, mode='reflect')
    # Frame Signal
    framed_series = tf.signal.frame(padded_series,
                                    fr_length,
                                    hop_length,
                                    pad_end=True)
    framed_series = framed_series.numpy()
    framed_series = framed_series.astype(np.float64)
    # Specify window function
    window = get_window("triang", fr_length, fftbins=False)
    # Multiply each frame with specified window
    for fr_idx in range(0, framed_series.shape[0]):
        framed_series[fr_idx, :] *= window
    return framed_series


def unframe_time_series(framed_time_series, hop_size, series_length):
    '''
    A function to divide signal into overlaping frames.
      
    Parameters
    ----------
    fr_length : int 
        Frame length. 
    hop_size : int 
        Number of samples to hop in each overlaping frame.
    series_length : int
        Original time series length.
    Output
    ------
    framed_time_series : np.array (no_frames, fr_length)
        The framed time series signal 
    '''
    # Overlap and add series
    added_signal = tf.signal.overlap_and_add(framed_time_series, hop_size)
    added_signal = added_signal.numpy()
    return added_signal[hop_size:series_length + hop_size]


def frame_length(level):
    '''
    Function to determine frame and hop length according to
        the decomposition level
        
        Parameters
        ----------
            level : int
                Decomposition level of the wavelet
                    transform
        Returns
        -------
            fr_length : int
                Frame length in the specified 
                    decomposition level
            hop_length : int
                Hop length in the specified 
                    decomposition level
    '''
    fr_length = int(config.n_fft // np.power(2, level))
    hop_length = int(fr_length / 2)
    return fr_length, hop_length


def filter_signal(wp, gb):
    '''
    Function to filter a noisy audio signal according to the given weights 
        per band.
        
        Parameters 
        ----------
            wp : np.ndarray
                Wavelet packet transform of the noisy signal
            gb : numpy.ndarray
                Gains for each frequency band.
        Returns
        -------
            signal : np.ndarray
                Filtered signal.
    '''
    # Calculate time series number of frames to filter the signal with.
    no_frames = gb.shape[0]
    # Iterate througn all filtering bands
    for band_idx in range(0, len(mel_wavelet_bands)):
        # Get band time series data
        wavelet_band_data = wp[mel_wavelet_bands[band_idx]].data
        band_level = wp[mel_wavelet_bands[band_idx]].level
        # Calculate frame length according to wavelet band level
        fr_length, hop_length = frame_length(band_level)
        # Frame band time series data
        band_framed = frame_time_series(wavelet_band_data, fr_length,
                                        hop_length)

        # Multiply band with the corresponding gains
        band_framed[0:no_frames, :] *= np.reshape(gb[:, band_idx],
                                                  (no_frames, 1))
        # Filter last frame too
        if (band_framed.shape[0] > no_frames):
            band_framed[-1, :] *= gb[-1, band_idx]

        # Unframe band time series data
        band_data = unframe_time_series(band_framed, hop_length,
                                        wavelet_band_data.shape[0])
        # Put filtered data in band node
        wp[mel_wavelet_bands[band_idx]].data = band_data
    # Return filtered signal
    signal = wp.reconstruct(update=True)
    return signal


def feature_extraction(clean_audio_file_absolute_path,
                       noisy_audio_file_absolute_path):
    '''This is the main feature extraction function for one file.
        Here we compute the Noisy Wavelet Cepstral Coefficients,
        deltas and deltas-deltas of them. Also we compute the 
        ideal gains and Voice Activity Detection non Linearity.
        
        Parameters 
        ----------
            clean_audio_file_absolute_path : str
                Folder Path for clean audio file.
            noisy_audio_file_absolute_path : str
                Folder Path for noisy audio file.
           
        Returns
        -------
            WTCC : numpy.ndarray 
                Mel Scaled Wavelet Cepstral Coefficients of the noisy signal.
            WTCC_delta : numpy.ndarray
                WTCCs delta features; A local estimate of the derivative of the 
                input data along the time axis.
            WTCC_delta_delta : numpy.ndarray
                WTCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb : numpy.ndarray
                Ideal gains for each frequency band.
            VAD : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 
    '''
    # Load audio signals
    clean_signal, sr = librosa.load(clean_audio_file_absolute_path,
                                    sr=config.sr)
    noisy_signal, sr = librosa.load(noisy_audio_file_absolute_path,
                                    sr=config.sr)

    # Normalize signals
    clean_signal = librosa.util.normalize(clean_signal)
    noisy_signal = librosa.util.normalize(noisy_signal)

    # Calculate VAD non - linearity
    fr_length, hop_length = frame_length(level=0)
    RMS_frame_energy = librosa.feature.rms(y=clean_signal,
                                           frame_length=fr_length,
                                           hop_length=hop_length,
                                           center=True)
    VAD_threshold = 1e-1
    VAD = (RMS_frame_energy > VAD_threshold).astype(int)
    VAD = np.transpose(VAD)

    # Calculate time series no_frames
    no_frames = VAD.shape[0]

    # Calculate wavelet packet trasform for each signal (clean & noisy)
    wp_clean = pywt.WaveletPacket(data=clean_signal,
                                  wavelet=config.wavelet,
                                  mode='symmetric')
    wp_noisy = pywt.WaveletPacket(data=noisy_signal,
                                  wavelet=config.wavelet,
                                  mode='symmetric')

    # Initialize gb and WTTC arrays
    gb = np.zeros((no_frames, len(mel_wavelet_bands)), dtype="float32")
    WTCC = gb

    # Iterate througn all filtering bands
    for band_idx in range(0, len(mel_wavelet_bands)):
        # Get band time series data for clean and noisy speach
        clean_ts_data = wp_clean[mel_wavelet_bands[band_idx]].data
        noisy_ts_data = wp_noisy[mel_wavelet_bands[band_idx]].data

        # Calculate frame length according to wavelet band level
        band_level = wp_clean[mel_wavelet_bands[band_idx]].level
        fr_length, hop_length = frame_length(band_level)

        # Frame band time series data for clean and noisy
        clean_band_framed = frame_time_series(clean_ts_data, fr_length,
                                              hop_length)
        noisy_band_framed = frame_time_series(noisy_ts_data, fr_length,
                                              hop_length)

        # Square band data for clean and noisy
        clean_band_framed = np.square(clean_band_framed)
        noisy_band_framed = np.square(noisy_band_framed)
        # Calculate mean
        clean_band_energy = np.mean(clean_band_framed, axis=1)
        noisy_band_energy = np.mean(noisy_band_framed, axis=1)

        # Put Powered scalogram coeff in WTCC array
        WTCC[:, band_idx] = noisy_band_energy[0:no_frames]

        # Calculate ideal gains
        gb[:, band_idx] = np.sqrt(clean_band_energy[0:no_frames] /
                                  noisy_band_energy[0:no_frames])

    # Convert power scalogram to db scalogram.
    WTCC = librosa.power_to_db(np.transpose(WTCC))
    # Compute dct transform along frequency axis.
    WTCC = dct(WTCC, axis=0, type=2, norm="ortho")

    # Compute WTCC deltas #
    n_delta_coef = config.n_delta_coef
    WTCC_delta = librosa.feature.delta(WTCC)
    WTCC_delta = WTCC_delta[:n_delta_coef, :]
    # Compute MFCC deltas - deltas #
    WTCC_delta_delta = librosa.feature.delta(WTCC, order=2)
    WTCC_delta_delta = WTCC_delta_delta[:n_delta_coef, :]

    # Transpose arrays back to desired shape
    WTCC = np.transpose(WTCC)
    WTCC_delta = np.transpose(WTCC_delta)
    WTCC_delta_delta = np.transpose(WTCC_delta_delta)

    # Normalize gains gb to [0,1]
    #gb = librosa.util.normalize(gb,axis=1)

    return WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD


def Edinburg_noisy_dataset(set='train'):
    '''Main feature extraction fuction for the 
        Edinburg noisy speech dataset.

        Parameters
        ----------
        set : str
            Dataset to be created. Accepted values are 'train' and 'test'.
    '''
    if (set == 'train'):
        # Specify train set audio path
        clean_audio_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/clean_trainset_56spk_wav/'
        noisy_audio_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/noisy_trainset_56spk_wav/'
        logfile_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/log_trainset_56spk.txt'

    if (set == 'test'):
        # Specify  test set audio path
        clean_audio_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/clean_testset_wav/'
        noisy_audio_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/noisy_testset_wav/'
        logfile_path = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Edinburgh_Noisy_Speech_Dataset/log_testset.txt'

    # Get input and output features
    log_data, WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = gen_Edinburg_noisy_dataset(
        clean_audio_path, noisy_audio_path, logfile_path)

    # Save datasets features for training
    np.savez("X_" + set + ".npz",
             WTCC=WTCC,
             WTCC_delta=WTCC_delta,
             WTCC_delta_delta=WTCC_delta_delta)
    np.savez("y_" + set + ".npz", gb=gb, VAD=VAD)
    np.savez("logdata_" + set + ".npz", log_data=log_data)


def gen_Edinburg_noisy_dataset(clean_audio_path, noisy_audio_path,
                               logfile_path):
    '''Helper function to generate the wavelet features 
        for all the files in the Edinburg noisy speech dataset.

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio files.
            noisy_audio_path : str
                Folder Path for noisy audio files.
            logfile_path : str
                Folder Path for the logfile.
        Returns
        -------
            logdata_list : list
                List with the log file data for each track. Each list entry 
                contains : ['name of the track' 'noise enviroment' 'SNR']
                    e.g : ['p232_005.wav' 'bus' '2.500000e+00']
            noisy_WTCC_arr : numpy.ndarray 
                Wavelet transform cepstral coefficients of the noisy signal.
            WTCC_delta_arr : numpy.ndarray
                WTCCs delta features; A local estimate of the derivative 
                of the input data along the time axis.
            WTCC_delta_delta_arr : numpy.ndarray
                WTCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb_arr : numpy.ndarray
                Ideal gains for each frequency band.
            VAD_arr : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 

    '''

    logdata_list = []
    # Input features
    noisy_WTCC_list = []
    WTCC_delta_list = []
    WTCC_delta_delta_list = []
    # Output features
    gb_list = []
    VAD_list = []

    # Open log file and start reading
    logfile = open(logfile_path, "r")

    for line in logfile:
        # Split line attributes
        att = line.split()
        att[0] = att[0] + '.wav'
        audio_file = att[0]
        # Compute absolute paths for one file
        clean_audio_file_absolute_path = (clean_audio_path + audio_file)
        noisy_audio_file_absolute_path = (noisy_audio_path + audio_file)
        # Extract input and output features
        noisy_WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = feature_extraction(
            clean_audio_file_absolute_path, noisy_audio_file_absolute_path)

        # Append information and features
        logdata_list.append(att)
        # Input features
        noisy_WTCC_list.append(noisy_WTCC)
        WTCC_delta_list.append(WTCC_delta)
        WTCC_delta_delta_list.append(WTCC_delta_delta)
        # Output features
        gb_list.append(gb)
        VAD_list.append(VAD)

    # Trasform ALL lists to numpy array in order to save them
    # Input Features
    noisy_WTCC_arr = transform_list_to_nparray(noisy_WTCC_list)
    WTCC_delta_arr = transform_list_to_nparray(WTCC_delta_list)
    WTCC_delta_delta_arr = transform_list_to_nparray(WTCC_delta_delta_list)
    # Output features
    gb_arr = transform_list_to_nparray(gb_list)
    VAD_arr = transform_list_to_nparray(VAD_list)

    return logdata_list, noisy_WTCC_arr, WTCC_delta_arr, WTCC_delta_delta_arr, gb_arr, VAD_arr


def MS_SNSD(set='train'):
    '''Main feature extraction fuction for the 
        Microsoft scalable noisy npeech dataset.

        Parameters
        ----------
        set : str
            Dataset to be created. Accepted values are 'train' and 'test'.
    '''

    dataset_dir = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/MS-SNSD/'
    if (set == 'train'):
        # Specify train set audio path

        clnsp_dir = os.path.join(dataset_dir, 'CleanSpeech_training')
        nsp_dir = os.path.join(dataset_dir, 'NoisySpeech_training')

    if (set == 'test'):
        # Specify  test set audio path
        clnsp_dir = os.path.join(dataset_dir, 'CleanSpeech_testing')
        nsp_dir = os.path.join(dataset_dir, 'NoisySpeech_testing')

    # Get input and output features
    log_data, WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = gen_MS_SNSD_dataset(
        clnsp_dir, nsp_dir)

    # Save datasets features for training
    np.savez("X_" + set + ".npz",
             WTCC=WTCC,
             WTCC_delta=WTCC_delta,
             WTCC_delta_delta=WTCC_delta_delta)
    np.savez("y_" + set + ".npz", gb=gb, VAD=VAD)
    np.savez("logdata_" + set + ".npz", log_data=log_data)


def gen_MS_SNSD_dataset(clnsp_dir, nsp_dir):
    '''Helper function to generate the wavelet features
        for all the files in the Microsoft scalable noisy speech dataset.

        Parameters
        ----------
            clnsp_dir : str
                Folder directory for clean audio files.
            nsp_dir : str
                Folder directory for noisy audio files.
        Returns
        -------
            logdata_list : list
                List with the log file data for each track. Each list entry 
                contains : ['noise file name' 'noise enviroment' 'SNR' 'clean speech file name'].
                For this dataset the noisy speech file name contains all the valiable info.
                    e.g : ['noisy3104_SNRdb_-5.0_clnsp3104.wav']
            noisy_WTCC_arr : numpy.ndarray 
                Wavelet transform cepstral coefficients of the noisy signal.
            WTCC_delta_arr : numpy.ndarray
                WTCCs delta features; A local estimate of the derivative 
                of the input data along the time axis.
            WTCC_delta_delta_arr : numpy.ndarray
                WTCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb_arr : numpy.ndarray
                Ideal gains for each frequency band.
            VAD_arr : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 

    '''

    logdata_list = []
    # Input features
    noisy_WTCC_list = []
    WTCC_delta_list = []
    WTCC_delta_delta_list = []
    # Output features
    gb_list = []
    VAD_list = []

    # Audio format to read, we use it to read all *.wav files in a directory.
    audioformat = '*.wav'
    # Get nsp filenames (absolute_paths) of all noisy *.wav files in noisy speech directory
    nsp_filenames = glob.glob(os.path.join(nsp_dir, audioformat))

    # Iterate through each noisy sample
    for idx_n in range(0, len(nsp_filenames)):
        # Get clean speech audio file name
        clnsp_audiofile = nsp_filenames[idx_n].split("_")[-1]
        # Get clean speech filename (absolute_path)
        clnsp_filename = os.path.join(clnsp_dir, clnsp_audiofile)

        # Extract input and output features for one file
        noisy_WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = feature_extraction(
            clnsp_filename, nsp_filenames[idx_n])

        # Get all info from noisy speech audio file
        nsp_filename = nsp_filenames[idx_n].split("/")[-1]
        logdata_list.append(nsp_filename)

        # Input features
        noisy_WTCC_list.append(noisy_WTCC)
        WTCC_delta_list.append(WTCC_delta)
        WTCC_delta_delta_list.append(WTCC_delta_delta)
        # Output features
        gb_list.append(gb)
        VAD_list.append(VAD)

    # Trasform ALL lists to numpy array in order to save them
    # Input Features
    noisy_WTCC_arr = transform_list_to_nparray(noisy_WTCC_list)
    WTCC_delta_arr = transform_list_to_nparray(WTCC_delta_list)
    WTCC_delta_delta_arr = transform_list_to_nparray(WTCC_delta_delta_list)
    # Output features
    gb_arr = transform_list_to_nparray(gb_list)
    VAD_arr = transform_list_to_nparray(VAD_list)

    return logdata_list, noisy_WTCC_arr, WTCC_delta_arr, WTCC_delta_delta_arr, gb_arr, VAD_arr


def example_dataset(set='example'):
    '''Just an example function for dataset generation, nothing more.
    '''

    clean_audio_path = './example_files/clean/'
    noisy_audio_path = './example_files/noisy/'
    logfile_path = './example_files/log_example.txt'

    # Example of the generated dataset
    log_data, WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = gen_Edinburg_noisy_dataset(
        clean_audio_path, noisy_audio_path, logfile_path)
    # Save training data
    np.savez("./example_files/generated_dataset/X_wavelets_" + set + ".npz",
             WTCC=WTCC,
             WTCC_delta=WTCC_delta,
             WTCC_delta_delta=WTCC_delta_delta)
    np.savez("./example_files/generated_dataset/y_" + set + ".npz",
             gb=gb,
             VAD=VAD)
    np.savez("./example_files/generated_dataset/logdata_" + set + ".npz",
             log_data=log_data)

    # LOAD Data
    log_data = np.load("./example_files/generated_dataset/logdata_example.npz",
                       allow_pickle=True)
    att = log_data["log_data"]
    print(att[:, 2])


def example_of_filtering():
    '''Example function (just for presentation).
        Here we take a batch of clean and a noisy audio signals. 
        We compute the ideal gains and then filter each signal with them.

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio file.
            noisy_audio_path : str
                Folder Path for noisy audio file.
    '''
    # Folder Path for clean & noisy audio files.
    clean_audio_path = './example_files/clean/'
    noisy_audio_path = './example_files/noisy/'
    # List ALL files in the data paths
    clean_audio_files = os.listdir(clean_audio_path)
    noisy_audio_files = os.listdir(noisy_audio_path)

    # Filter each of the files
    for audio_file in clean_audio_files:
        print(audio_file)
        # Find absolute paths
        clean_audio_file_absolute_path = (clean_audio_path + audio_file)
        noisy_audio_file_absolute_path = (noisy_audio_path + audio_file)
        # Calculate wavelet packet trasform for the noisy signal
        wp_noisy = waveletPacketTransform(noisy_audio_file_absolute_path)
        # Extract input and output features
        WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = feature_extraction(
            clean_audio_file_absolute_path, noisy_audio_file_absolute_path)
        # Filter Signal with predicted Gains
        filtered_signal = filter_signal(wp_noisy, gb)
        sf.write('./example_files/filtered/' + str(audio_file),
                 filtered_signal, config.sr)


def transform_list_to_nparray(list):
    '''Just a HELPER function for gen_dataset that transforms a list to 
        numpy array of different dimensions.

        Parameters
        ----------
        list : 
            The list to be converted to numpy array.
        Returns
        -------
        arr : 
            The converted numpy array.
    '''

    list_len = len(list)
    arr = np.empty(list_len, object)
    arr[:] = list
    return arr


if __name__ == "__main__":
    ''' Make an example of filtering samples.
    Results @ : ./example_files/filtered
    '''

    #example_of_filtering()
    '''Here we generate train and test for each of the datasets.
        For example : train feature extraction results are stored 
        @ : ./X_train.npz
            ./y_train.npz
            ./logdata.npz
    '''
    # Generate train set.
    #Edinburg_noisy_dataset(set = 'train')
    MS_SNSD()

    # Generate test set
    #Edinburg_noisy_dataset(set = 'test')
    #MS_SNSD(set = 'test')
