import librosa
import librosa.display as dsp
import matplotlib.pyplot as plt
import numpy as np

snr = "17.5dB/"
PESQ = "PESQ_low/"
path = snr + PESQ
noisy_path = path+"p257_001_noisy.wav"
filtered_path = path+"p257_001_filtered.wav"
clean_path = path+"p257_001_clean.wav"

noisy, sr = librosa.load(noisy_path)
filtered, sr = librosa.load(filtered_path)
clean, sr = librosa.load(clean_path)

noisy = librosa.util.normalize(noisy)
filtered = librosa.util.normalize(filtered)
clean = librosa.util.normalize(clean)

# Display filtered waveform
librosa.display.waveplot(noisy, sr=sr, color='r',alpha=0.3);
librosa.display.waveplot(filtered, sr=sr, color='b')
plt.show()

# Display clean waveform
plt.plot()
librosa.display.waveplot(clean, sr=sr, color='g')
plt.show()

# Display noisy waveform
plt.plot()
librosa.display.waveplot(noisy, sr=sr, color='r')
plt.show()
