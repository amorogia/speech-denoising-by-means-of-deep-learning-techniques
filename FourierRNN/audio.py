import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
import numpy.matlib
import soundfile as sf
from scipy.io import wavfile
import os
import config
import glob


def compute_stft(audio_path):
    '''A function to :
        1. Load Audio Signal
        2. Normalize Audio Signal
        3. Compute STFT of the audio signal according to config.py specifications.
             
        Parameters 
        ----------
            audio_path : str
                Folder path of an audio file.
        Output
        ------
            STFT : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients. 
                    (see librosa.org for more info)
    '''

    # Load Signal
    signal, sr = librosa.load(audio_path, sr=config.sr)
    # Normalize Signal
    signal = librosa.util.normalize(signal)
    # Compute STFT
    STFT = librosa.stft(signal,
                        n_fft=config.n_fft,
                        hop_length=config.hop_length)
    return STFT


def load_normalize_stft(clean_audio_path, noisy_audio_path):
    '''Helper function for feature_extraction function. 
            We just use compute_stft in both noisy and clean signal 

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio file.
            noisy_audio_path : str
                Folder Path for noisy audio file.
        Returns 
        -------
            stft_clean_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients 
                for the clean signal (see librosa.org for more info).
            stft_noisy_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients 
                for the noisy signal (see librosa.org for more info).
    '''

    stft_clean_signal = compute_stft(clean_audio_path)
    stft_noisy_signal = compute_stft(noisy_audio_path)

    return stft_clean_signal, stft_noisy_signal


def feature_extraction(stft_clean_signal, stft_noisy_signal):
    '''This is the main feature extraction function.
        Here we compute the Noisy Mel Frequency Coefficient,
        deltas and deltas-deltas of them. Also we compute the 
        ideal gains and Voice Activity Detection non Linearity.
        
        Parameters 
        ----------
            stft_clean_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients
                for the clean signal (see librosa.org for more info).
                
            stft_noisy_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients
                for the noisy signal (see librosa.org for more info).

        Returns
        -------
            noisy_mfccs : numpy.ndarray 
                Mel Frequency Cepstral Coefficients of the noisy signal.
            mfcc_delta : numpy.ndarray
                MFCCs delta features; A local estimate of the derivative of the 
                input data along the time axis.
            mfcc_delta_delta : numpy.ndarray
                MFCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb : numpy.ndarray
                Ideal gains for each frequency band.
            VAD : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 
    '''

    n_delta_coef = config.n_delta_coef

    # Compute mel - filterbanks
    mels = librosa.filters.mel(config.sr,
                               n_fft=config.n_fft,
                               n_mels=config.n_mels,
                               norm=None)
    # Compute ideal gains : gb #
    # Keep the amplitude of STFT
    S_noisy = np.abs(stft_noisy_signal)
    S_clean = np.abs(stft_clean_signal)
    # Compute energy
    energy_clean = np.matmul(mels, S_clean**2)
    energy_noisy = np.matmul(mels, S_noisy**2)
    # Compute gains
    gb = np.sqrt(energy_clean / energy_noisy)
    # Normalize gains gb to [-1,1]
    gb = librosa.util.normalize(gb)

    # Compute MFCC's for noisy signal #
    # Power - Spectogram of noisy signal
    D_noisy = S_noisy**2
    # Mel - Spectogram of noisy Signal
    mel_spectogram_noisy = librosa.feature.melspectrogram(
        S=D_noisy,
        sr=config.sr,
        n_fft=config.n_fft,
        hop_length=config.hop_length,
        n_mels=config.n_mels)
    # MFCCs of noisy signal
    noisy_mfccs = librosa.feature.mfcc(
        sr=config.sr,
        S=librosa.power_to_db(mel_spectogram_noisy),
        n_mfcc=config.n_mels)

    # Compute MFCC deltas #
    #n_delta_coef=6
    mfcc_delta = librosa.feature.delta(noisy_mfccs)
    mfcc_delta = mfcc_delta[:n_delta_coef, :]
    # Compute MFCC deltas - deltas #
    mfcc_delta_delta = librosa.feature.delta(noisy_mfccs, order=2)
    mfcc_delta_delta = mfcc_delta_delta[:n_delta_coef, :]

    # Compute VAD non - linearity
    RMS_frame_energy = librosa.feature.rms(S=S_clean,
                                           frame_length=config.n_fft,
                                           hop_length=config.hop_length)
    VAD_threshold = 1e-1
    VAD = (RMS_frame_energy > VAD_threshold).astype(int)

    return noisy_mfccs, mfcc_delta, mfcc_delta_delta, gb, VAD


def filter_signal(stft_noisy_signal, gb):
    '''Function to filter a noisy audio signal according to the given weights 
        per band.
        
        Parameters 
        ----------
            stft_noisy_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients
                for the noisy signal.
            gb : numpy.ndarray
                Gains for each frequency band.
        Returns
        -------
            stft_filtered_signal : np.ndarray
                Complex-valued matrix of short-term Fourier transform coefficients
                for the filtered signal.
    '''

    # Compute mel - filterbanks
    mels = librosa.filters.mel(config.sr,
                               n_fft=config.n_fft,
                               n_mels=config.n_mels,
                               norm=None)
    # Keep the magnitude of the STFT
    S = np.abs(stft_noisy_signal)

    # Filter each frame
    for j in range(1, len(S.T)):
        frame = S[:, j]
        # Initialize filt_frame
        filt_frame = np.zeros(frame.shape)
        for i in range(config.n_mels):
            filter = mels[i, :] * gb[i, j]
            filt_frame += frame * filter
        S[:, j] = filt_frame

    # Reconstruct signal with the original phase
    stft_filtered_signal = S * np.exp(1j * np.angle(stft_noisy_signal))
    return stft_filtered_signal


def example_of_filtering(clean_audio_path, noisy_audio_path):
    '''Example function (just for presentation).
        Here we take a batch of clean and a noisy audio signals. 
        We compute the ideal gains and then filter each signal with them.

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio file.
            noisy_audio_path : str
                Folder Path for noisy audio file.
    '''

    # List ALL files in the data paths
    clean_audio_files = os.listdir(clean_audio_path)
    noisy_audio_files = os.listdir(noisy_audio_path)

    # Filter each of the files
    for audio_file in clean_audio_files:
        # Find absolute paths
        clean_audio_file_absolute_path = (clean_audio_path + audio_file)
        noisy_audio_file_absolute_path = (noisy_audio_path + audio_file)
        # Compute STFT
        stft_clean_signal, stft_noisy_signal = load_normalize_stft(
            clean_audio_file_absolute_path, noisy_audio_file_absolute_path)
        # Feature Extraction
        noisy_mfccs, mfcc_delta, mfcc_delta_delta, gb, VAD = feature_extraction(
            stft_clean_signal, stft_noisy_signal)
        # Filter Signal with ideal gains
        stft_filtered_signal = filter_signal(stft_noisy_signal, gb)
        # Load clean signal to get the exact length of it
        clean_signal, sr = librosa.load(clean_audio_file_absolute_path,
                                        sr=config.sr)
        # Save signal
        y_filtered = librosa.istft(stft_filtered_signal,
                                   hop_length=config.hop_length,
                                   win_length=config.n_fft,
                                   length=len(clean_signal))
        sf.write('./example_files/filtered/' + str(audio_file), y_filtered,
                 config.sr)


def gen_dataset(clean_audio_path, noisy_audio_path, logfile_path):
    '''Main function to generate the datasets.

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio files.
            noisy_audio_path : str
                Folder Path for noisy audio files.
            logfile_path : str
                Folder Path for the logfile.
        Returns
        -------
            logdata_list : list
                List with the log file data for each track. Each list entry 
                contains : ['name of the track' 'noise enviroment' 'SNR']
                    e.g : ['p232_005.wav' 'bus' '2.500000e+00']
            noisy_mfccs_arr : numpy.ndarray 
                Mel Frequency Cepstral Coefficients of the noisy signal.
            mfcc_delta_arr : numpy.ndarray
                MFCCs delta features; A local estimate of the derivative 
                of the input data along the time axis.
            mfcc_delta_delta_arr : numpy.ndarray
                MFCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb_arr : numpy.ndarray
                Ideal gains for each frequency band.
            VAD_arr : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 

    '''

    logdata_list = []
    # Input features
    noisy_mfccs_list = []
    mfcc_delta_list = []
    mfcc_delta_delta_list = []
    # Output features
    gb_list = []
    VAD_list = []

    # List ALL files in the data paths
    #clean_audio_files = os.listdir(clean_audio_path)
    #noisy_audio_files = os.listdir(noisy_audio_path)

    # Open log file and start reading
    logfile = open(logfile_path, "r")

    for line in logfile:
        # Split line attributes
        att = line.split()
        att[0] = att[0] + '.wav'
        audio_file = att[0]
        clean_audio_file_absolute_path = (clean_audio_path + audio_file)
        noisy_audio_file_absolute_path = (noisy_audio_path + audio_file)
        # Compute STFT of clean and noisy signals
        stft_clean_signal, stft_noisy_signal = load_normalize_stft(
            clean_audio_file_absolute_path, noisy_audio_file_absolute_path)
        # Extract input and output features
        noisy_mfccs, mfcc_delta, mfcc_delta_delta, gb, VAD = feature_extraction(
            stft_clean_signal, stft_noisy_signal)

        # Append information and features
        logdata_list.append(att)
        # Input features
        noisy_mfccs_list.append(noisy_mfccs)
        mfcc_delta_list.append(mfcc_delta)
        mfcc_delta_delta_list.append(mfcc_delta_delta)
        # Output features
        gb_list.append(gb)
        VAD_list.append(VAD)

    # Trasform ALL lists to numpy array in order to save them
    # Input Features
    noisy_mfccs_arr = transform_list_to_nparray(noisy_mfccs_list)
    mfcc_delta_arr = transform_list_to_nparray(mfcc_delta_list)
    mfcc_delta_delta_arr = transform_list_to_nparray(mfcc_delta_delta_list)
    # Output features
    gb_arr = transform_list_to_nparray(gb_list)
    VAD_arr = transform_list_to_nparray(VAD_list)

    return logdata_list, noisy_mfccs_arr, mfcc_delta_arr, mfcc_delta_delta_arr, gb_arr, VAD_arr


def MS_SNSD(set='train'):
    '''
    Feature extraction fuction for the Microsoft Scalable Noisy Speech Dataset.
    '''
    dataset_dir = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/Reverberation Dataset/'
    if (set == 'train'):
        # Specify train set audio path

        clnsp_dir = os.path.join(dataset_dir, 'CleanSpeech_training')
        nsp_dir = os.path.join(dataset_dir, 'NoisySpeech_training')

    if (set == 'test'):
        # Specify  test set audio path
        clnsp_dir = os.path.join(dataset_dir, 'CleanSpeech_testing')
        nsp_dir = os.path.join(dataset_dir, 'NoisySpeech_testing')

    # Get input and output features
    log_data, WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = gen_MS_SNSD_dataset(
        clnsp_dir, nsp_dir)

    # Save datasets features for training
    np.savez("X_" + set + ".npz",
             WTCC=WTCC,
             WTCC_delta=WTCC_delta,
             WTCC_delta_delta=WTCC_delta_delta)
    np.savez("y_" + set + ".npz", gb=gb, VAD=VAD)
    np.savez("logdata_" + set + ".npz", log_data=log_data)


def gen_MS_SNSD_dataset(clnsp_dir, nsp_dir):
    '''Main function to generate the datasets.

        Parameters
        ----------
            clnsp_dir : str
                Folder Path for clean audio files.
            nsp_dir : str
                Folder Path for noisy audio files.
            logfile_path : str
                Folder Path for the logfile.
        Returns
        -------
            logdata_list : list
                List with the log file data for each track. Each list entry 
                contains : ['noise file name' 'noise enviroment' 'SNR' 'clean speech file name'].
                For this dataset the noisy speech file name contains all the valiable info.
                    e.g : ['noisy3104_SNRdb_-5.0_clnsp3104.wav']
            noisy_mfccs_arr : numpy.ndarray 
                Mel Frequency Cepstral Coefficients of the noisy signal.
            mfcc_delta_arr : numpy.ndarray
                MFCCs delta features; A local estimate of the derivative 
                of the input data along the time axis.
            mfcc_delta_delta_arr : numpy.ndarray
                MFCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb_arr : numpy.ndarray
                Ideal gains for each frequency band.
            VAD_arr : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 

    '''

    logdata_list = []
    # Input features
    noisy_mfccs_list = []
    mfcc_delta_list = []
    mfcc_delta_delta_list = []
    # Output features
    gb_list = []
    VAD_list = []

    # Audio format to read, we use it to read all *.wav files in a directory.
    audioformat = '*.wav'
    # Get nsp filenames (absolute_paths) of all noisy *.wav files in noisy speech directory
    nsp_filenames = glob.glob(os.path.join(nsp_dir, audioformat))
    print(nsp_filenames)

    # Iterate through each noisy sample
    for idx_n in range(0, len(nsp_filenames)):
        # Get clean speech audio file name
        clnsp_audiofile = nsp_filenames[idx_n].split("_")[-1]
        # Get clean speech filename (absolute_path)
        clnsp_filename = os.path.join(clnsp_dir, clnsp_audiofile)

        print(clnsp_filename, nsp_filenames[idx_n])
        # Compute STFT of clean and noisy signals
        stft_clean_signal, stft_noisy_signal = load_normalize_stft(
            clnsp_filename, nsp_filenames[idx_n])
        # Extract input and output features
        noisy_mfccs, mfcc_delta, mfcc_delta_delta, gb, VAD = feature_extraction(
            stft_clean_signal, stft_noisy_signal)
        # Get all info from noisy speech audio file
        nsp_filename = nsp_filenames[idx_n].split("/")[-1]
        logdata_list.append(nsp_filename)
        # Input features
        noisy_mfccs_list.append(noisy_mfccs)
        mfcc_delta_list.append(mfcc_delta)
        mfcc_delta_delta_list.append(mfcc_delta_delta)
        # Output features
        gb_list.append(gb)
        VAD_list.append(VAD)

    # Trasform ALL lists to numpy array in order to save them
    # Input Features
    noisy_mfccs_arr = transform_list_to_nparray(noisy_mfccs_list)
    mfcc_delta_arr = transform_list_to_nparray(mfcc_delta_list)
    mfcc_delta_delta_arr = transform_list_to_nparray(mfcc_delta_delta_list)
    # Output features
    gb_arr = transform_list_to_nparray(gb_list)
    VAD_arr = transform_list_to_nparray(VAD_list)

    return logdata_list, noisy_mfccs_arr, mfcc_delta_arr, mfcc_delta_delta_arr, gb_arr, VAD_arr


def transform_list_to_nparray(list):
    '''Just a HELPER function for gen_dataset that transforms a list to 
        numpy array of different dimensions.

        Parameters
        ----------
        list : 
            The list to be converted to numpy array.
        Returns
        -------
        arr : 
            The converted numpy array.
    '''

    list_len = len(list)
    arr = np.empty(list_len, object)
    arr[:] = list
    return arr


if __name__ == "__main__":
    # Generate train sets

    #Edinburg_noisy_dataset(set = 'train')
    #MS_SNSD(set = 'train')

    # Generate test set
    #Edinburg_noisy_dataset(set = 'test')
    #MS_SNSD(set = 'test')
    MS_SNSD(set='test')
