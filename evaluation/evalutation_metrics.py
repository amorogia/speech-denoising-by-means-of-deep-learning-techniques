import librosa
import pysepm
import os
import glob
import numpy as np


def calculate_PESQ(clean_speech, enhanced_speech, sr):
    """ 
    Function to calculate PESQ score.
    Note that the sampling rate (sr) should be 16kHz for wideband.
    """

    PESQ_score = pysepm.pesq(clean_speech, enhanced_speech, sr)
    return PESQ_score[1]


def calculate_STOI(clean_speech, enhanced_speech, sr):
    """ 
    Function to calculate STOI score.
    Note that the sampling rate (sr) should be 16kHz.
    """

    STOI_score = pysepm.stoi(clean_speech, enhanced_speech, sr)
    return STOI_score


def calculate_NCM(clean_speech, enhanced_speech, sr):
    """ 
    Function to calculate STOI score.
    Note that the sampling rate (sr) should be 16kHz.
    """

    NCM_score = pysepm.ncm(clean_speech, enhanced_speech, sr)
    return NCM_score


def evaluate(clean_signal, filtered_signal):
    """ 
    Function to evaluate (STOI and PESQ scores) a single audio file.
    We should load the audio files in 16kHz.
    """

    # Specify 16kHz for wideband evaluation
    sr = 16000
    # Calculate metrics
    PESQ = calculate_PESQ(clean_signal, filtered_signal, sr)
    STOI = calculate_STOI(clean_signal, filtered_signal, sr)
    NCM = calculate_NCM(clean_signal, filtered_signal, sr)
    return PESQ, STOI, NCM


def get_noisy_audio_name(log_data):
    noisy_audio_name = log_data.split("/")[-1]
    return noisy_audio_name


def get_clean_audio_name(log_data):
    clean_audio_name = log_data.split("/")[-1]
    clean_audio_name = clean_audio_name.split("_")[-1]
    return clean_audio_name


def get_noise_type(log_data):
    noise_type = log_data.split("/")[-1]
    noise_type = noise_type.split("_")[0]
    return noise_type


def get_SNR(log_data):
    SNR = log_data.split("/")[-1]
    SNR = SNR.split("_")[-2]
    return SNR


if __name__ == "__main__":
    # Specify audio and logfile paths
    dataset_dir = '/content/gdrive/MyDrive/Diploma_thesis/se-code/datasets/MS-SNSD/'

    clean_audio_path = os.path.join(dataset_dir, 'CleanSpeech_testing')
    noisy_audio_path = os.path.join(dataset_dir, 'NoisySpeech_testing')
    #filtered_audio_path = os.path.join(dataset_dir, 'NoisySpeechFourierRNN_filtered')
    filtered_audio_path = os.path.join(dataset_dir, 'FourierRNN_filtered')

    # Initialize lists
    PESQ_RNNoise_list = []
    STOI_RNNoise_list = []
    NCM_RNNoise_list = []

    PESQ_Noisy_list = []
    STOI_Noisy_list = []
    NCM_Noisy_list = []

    PESQ_Clean_list = []
    STOI_Clean_list = []
    NCM_Clean_list = []

    log_data = []

    # Open log file and start reading
    audioformat = '*.wav'
    fltsp_filenames = glob.glob(os.path.join(filtered_audio_path, audioformat))

    for id in range(0, len(fltsp_filenames)):
        clean_audio_file_absolute_path = os.path.join(
            clean_audio_path, get_clean_audio_name(fltsp_filenames[id]))
        noisy_audio_file_absolute_path = os.path.join(
            noisy_audio_path, get_noisy_audio_name(fltsp_filenames[id]))
        filtered_audio_file_absolute_path = fltsp_filenames[id]

        print(clean_audio_file_absolute_path)
        print(noisy_audio_file_absolute_path)
        print(filtered_audio_file_absolute_path)

        # Specify 16kHz for wideband evaluation
        sr = 16000
        # Load Signal
        clean_signal, sr = librosa.load(clean_audio_file_absolute_path, sr=sr)
        noisy_signal, sr = librosa.load(noisy_audio_file_absolute_path, sr=sr)
        filtered_signal, sr = librosa.load(filtered_audio_file_absolute_path,
                                           sr=sr)

        # Calculate STOI and PESQ after RNNoise suppresion
        PESQ_RNNoise, STOI_RNNoise, NCM_RNNoise = evaluate(
            clean_signal, filtered_signal)
        # Append results to metrics list
        PESQ_RNNoise_list.append(PESQ_RNNoise)
        STOI_RNNoise_list.append(STOI_RNNoise)
        NCM_RNNoise_list.append(NCM_RNNoise)

        # Calculate STOI and PESQ of noisy audio file
        PESQ_Noisy, STOI_Noisy, NCM_Noisy = evaluate(clean_signal,
                                                     noisy_signal)
        # Append results to metrics list
        PESQ_Noisy_list.append(PESQ_Noisy)
        STOI_Noisy_list.append(STOI_Noisy)
        NCM_Noisy_list.append(NCM_Noisy)

        tmp = []
        tmp.append(get_noisy_audio_name(fltsp_filenames[id]))
        tmp.append(get_noise_type(fltsp_filenames[id]))
        tmp.append(get_SNR(fltsp_filenames[id]))

        log_data.append(tmp)

        # Print Results
        print("Audio Name : " + get_noisy_audio_name(fltsp_filenames[id]) +
              '\t' + "SNR : " + get_SNR(fltsp_filenames[id]))

        print("\t PESQ Metric : ")
        print("\t\t RNNoise = " + str(PESQ_RNNoise))
        print("\t\t Noisy = " + str(PESQ_Noisy))

        print("\t STOI Metric : ")
        print("\t\t RNNoise = " + str(STOI_RNNoise))
        print("\t\t Noisy = " + str(STOI_Noisy))

        print("\t NCM Metric : ")
        print("\t\t RNNoise = " + str(NCM_RNNoise))
        print("\t\t Noisy = " + str(NCM_Noisy))

    # Save evaluation Results
    np.savez("evaluation_results.npz",
             PESQ_RNNoise=PESQ_RNNoise_list,
             STOI_RNNoise=STOI_RNNoise_list,
             NCM_RNNoise=NCM_RNNoise_list,
             PESQ_Noisy=PESQ_Noisy_list,
             STOI_Noisy=STOI_Noisy_list,
             NCM_Noisy=NCM_Noisy_list)

    np.savez("logdata_eval.npz", log_data=log_data)
